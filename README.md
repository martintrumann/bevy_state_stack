# Bevy state stack
This crate allows you to use a state stack that is not confined to a single
stage.

## Bevy version
| bevy version | bevy_state_stack version |
| --- | --- |
| 0.8 | 0.1, 0.2 |

## Usage
You're better off importing the entire library.
```rust
use bevy_state_stack::*;
```

Add the state enum into the app.
```rust
app.add_state_stack(AppState::Menu)
```

Add different systems to the app.
```rust
app.add_system_on_enter(AppState::Menu, setup_menu)
	.add_system_on_exit(AppState::Menu, despawn::<Menu>)
	.add_system_on_enter(AppState::Map, setup_map)
	.add_system_on_update(AppState::Map, pause)
	.add_system_on_update(AppState::Encounter, pause)
	.add_system_set_on_update(
		AppState::Menu,
		SystemSet::new()
			.with_system(resume)
			.with_system(start_game),
	)
```

You can set, push, or pop the state on top of the stack
by inserting the `Stack` resource.
```rust
fn start_game(mut c: Commands) {
	c.insert_resource(Stack::Set(AppState::Map))
}

fn pause(mut c: Commands) {
	c.insert_resource(Stack::Push(AppState::Menu))
}

fn resume(mut c: Commands) {
	c.insert_resource(Stack::<AppState>::Pop);
}
```
